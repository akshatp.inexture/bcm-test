import React from "react";
import { Link } from "react-router-dom";
import Profile from "../assets/images/Profile.png";

const Header = () => {
    return (
        <header className="header-main">
            <div className="container">
                <div className="d-flex flex-column flex-md-row align-items-center p-2 px-md-2">
                    <Link to="/" className="mr-md-auto logo">
                        BCM<span>next</span>
                    </Link>
                    {/* <!-- <h5 className="my-0 mr-md-auto font-weight-normal">Company name</h5> --> */}
                    <ul className="d-flex  pl-0 my-2 my-md-0 align-items-center">
                        <li>
                            <Link className="text-dark" to="/">
                                <i className="fa fa-cog" aria-hidden="true"></i>
                            </Link>
                        </li>
                        <li>
                            <Link className="text-dark" to="/">
                                <i
                                    className="fa fa-bell"
                                    aria-hidden="true"
                                ></i>
                            </Link>
                        </li>
                        <li className="nav-item dropdown">
                            <Link
                                className="nav-link dropdown-toggle"
                                to="#"
                                id="navbarDropdownMenuLink"
                                role="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <img
                                    src={Profile}
                                    width="40"
                                    height="40"
                                    className="rounded-circle"
                                    alt="Profile"
                                />
                                <h3>
                                    Amara Kyle
                                    <p>BCM Manager</p>
                                </h3>
                            </Link>
                            <div
                                className="dropdown-menu"
                                aria-labelledby="navbarDropdownMenuLink"
                            >
                                <Link
                                    className="dropdown-item"
                                    to="/controlpanel"
                                >
                                    Dashboard
                                </Link>
                                <Link className="dropdown-item" to="/">
                                    Edit Profile
                                </Link>
                                <Link className="dropdown-item" to="/">
                                    Log Out
                                </Link>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
};

export default Header;
