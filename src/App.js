import { Route, Routes } from "react-router-dom";
import "./assets/css/style.css";
import ControlPanel from "./pages/ControlPanel";
import Header from "./components/Header";
import Home from "./pages/Home";

const App = () => {
    return (
        <>
            <Header />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/controlpanel" element={<ControlPanel />} />
            </Routes>
        </>
    );
};

export default App;
