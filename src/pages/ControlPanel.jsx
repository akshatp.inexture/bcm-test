import { useEffect } from "react";
import { Chart, Tooltip, Title, ArcElement, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import Manual from "../assets/images/manual.svg";
import Handshake from "../assets/images/handshake.svg";
import Interact from "../assets/images/interact.svg";
import Settings from "../assets/images/settings.svg";

Chart.register(Tooltip, Title, ArcElement, Legend);

const ControlPanel = () => {
  useEffect(() => {
    const cont1 = document.querySelector(".circular-container-part-1");
    const cont2 = document.querySelector(".circular-container-part-2");
    const cont3 = document.querySelector(".circular-container-part-3");
    const cont4 = document.querySelector(".circular-container-part-4");
    const cont = document.querySelectorAll(".container-opacity");

    const rectTopLeft = document.querySelector(".rect-top-left");
    const rectTopRight = document.querySelector(".rect-top-right");
    const rectBottomLeft = document.querySelector(".rect-bottom-left");
    const rectBottomRight = document.querySelector(".rect-bottom-right");
    const rect = document.querySelectorAll(".info-rect-opacity");

    const hide = () => {
      for (let i = 0; i < cont.length; i++) {
        cont[i].style.opacity = 0.5;
        cont[i].style.scale = 1;
      }
    };
    const hideInfoRect = () => {
      for (let i = 0; i < rect.length; i++) {
        rect[i].style.opacity = 0;
      }
    };

    cont1.addEventListener("mouseover", () => {
      hide();
      hideInfoRect();
      cont1.style.opacity = 1;
      rectTopLeft.style.opacity = 1;
    });
    cont2.addEventListener("mouseover", () => {
      hide();
      hideInfoRect();
      cont2.style.opacity = 1;
      rectTopRight.style.opacity = 1;
    });
    cont3.addEventListener("mouseover", () => {
      hide();
      hideInfoRect();
      cont3.style.opacity = 1;
      rectBottomLeft.style.opacity = 1;
    });
    cont4.addEventListener("mouseover", () => {
      hide();
      hideInfoRect();
      cont4.style.opacity = 1;
      rectBottomRight.style.opacity = 1;
    });

    cont1.addEventListener("mouseout", () => {
      cont1.style.opacity = 0.5;
      cont1.style.scale = 1;
      rectTopLeft.style.opacity = 0;
    });
    cont2.addEventListener("mouseout", () => {
      cont2.style.opacity = 0.5;
      cont2.style.scale = 1;
      rectTopRight.style.opacity = 0;
    });
    cont3.addEventListener("mouseout", () => {
      cont3.style.opacity = 0.5;
      cont3.style.scale = 1;
      rectBottomLeft.style.opacity = 0;
    });
    cont4.addEventListener("mouseout", () => {
      cont4.style.opacity = 0.5;
      cont4.style.scale = 1;
      rectBottomRight.style.opacity = 0;
    });
  }, []);

  useEffect(() => {
    const gaugeElement = document.querySelector(".gauge");
    const gaugeElement2 = document.querySelector(".gauge2");
    const gaugeElement3 = document.querySelector(".gauge3");
    const gaugeElement4 = document.querySelector(".gauge4");
    setGaugeValue(gaugeElement, 50, 1);
    setGaugeValue(gaugeElement2, 50, 2);
    setGaugeValue(gaugeElement3, 70, 3);
    setGaugeValue(gaugeElement4, 35, 4);
  }, []);

  function setGaugeValue(gauge, value, type) {
    if (type === 1) {
      if (value < 0 || value > 100) {
        return;
      }

      gauge.querySelector(".gauge__fill").style.transform = `rotate(${
        value / 400
      }turn)`;
      gauge.querySelector(".gauge__fill__text").textContent = `${Math.round(
        value
      )}%`;
    }
    if (type === 2) {
      if (value < 0 || value > 100) {
        return;
      }

      gauge.querySelector(".gauge__fill2").style.transform = `rotate(${
        value / 400
      }turn)`;
      gauge.querySelector(".gauge__fill__text2").textContent = `${Math.round(
        value
      )}%`;
    }
    if (type === 3) {
      if (value < 0 || value > 100) {
        return;
      }

      gauge.querySelector(".gauge__fill3").style.transform = `rotate(${
        value / 400
      }turn)`;
      gauge.querySelector(".gauge__fill__text3").textContent = `${Math.round(
        value
      )}%`;
    }

    if (type === 4) {
      if (value < 0 || value > 100) {
        return;
      }

      gauge.querySelector(".gauge__fill4").style.transform = `rotate(${
        value / 400
      }turn)`;
      gauge.querySelector(".gauge__fill__text4").textContent = `${Math.round(
        value
      )}%`;
    }
  }

  const data = {
    labels: false,
    datasets: [
      {
        type: "doughnut",
        label: "Dataset",
        data: [1, 1, 1, 1],
        backgroundColor: ["#9eb3fd"],
        hoverBackgroundColor: "#3E66FB",
        hoverOffset: 5,
        borderWidth: 0,
        borderRadius: 16,
        spacing: 30,
        cutout: "80%",
      },
    ],
  };

  const options = {
    cutoutPercentage: 80,
    responsive: true,
    plugins: {
      tooltip: {
        enabled: false,
      },
    },
  };

  return (
    <div className="wrapper d-flex justify-content-center align-items-center">
      <div className="chart__Wrapper">
        <Doughnut data={data} options={options}></Doughnut>
        <span className="label-1">DEFINE</span>
        <span className="label-1-1"> MODE</span>

        <span className="label-2"> DESIGN</span>

        <span className="label-3"> IMPROVE</span>
        <span className="label-4"> EMBED</span>

        <div className="guage__wrapper">
          <div className="gauge circular-container-part-1">
            <div className="gauge__body">
              <div className="gauge__fill">
                <p className="gauge__fill__text"></p>
              </div>
              <div className="gauge__cover"></div>
            </div>
          </div>
          <div className="gauge2 circular-container-part-2">
            <div className="gauge__body2">
              <div className="gauge__fill2">
                <p className="gauge__fill__text2"></p>
              </div>
              <div className="gauge__cover2"></div>
            </div>
          </div>
          <div className="gauge3 circular-container-part-3">
            <div className="gauge__body3">
              <div className="gauge__fill3">
                <p className="gauge__fill__text3"></p>
              </div>
              <div className="gauge__cover3"></div>
            </div>
          </div>
          <div className="gauge4 circular-container-part-4">
            <div className="gauge__body4">
              <div className="gauge__fill4">
                <p className="gauge__fill__text4"></p>
              </div>
              <div className="gauge__cover4"></div>
            </div>
          </div>
        </div>
        <div className="info__wrapper">
          <div className="quarter">
            <div className="quater_cover">
              <img src={Manual} alt="Manual" />
            </div>
          </div>
          <div className="quarter2">
            <div className="quater_cover2">
              <img src={Settings} alt="settings" />
            </div>
          </div>
          <div className="quarter3">
            <div className="quater_cover3">
              <img src={Handshake} alt="handshake" />
            </div>
          </div>
          <div className="quarter4">
            <div className="quater_cover4">
              <img src={Interact} alt="interact" />
            </div>
          </div>
        </div>
        <div className="infoRect">
          <div className="info-rect-opacity rect-top-left">
            <div className="infoContainer-t-l">
              <div className="rect-1">
                <div>
                  <h5>Policy</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Approved</p>
              </div>
              <div className="rect-2">
                <div>
                  <h5>Procedures</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Awaiting Approval</p>
              </div>
              <div className="rect-3">
                <div>
                  <h5>Playbook</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Not Approved</p>
              </div>
              <div className="rect-4">
                <div>
                  <h5>Appetite</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Not Approved</p>
              </div>
            </div>
          </div>

          <div className="info-rect-opacity rect-top-right">
            <div className="infoContainer-t-r">
              <div className="rect-1">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Policy</h5>
                </div>
                <p>Approved</p>
              </div>
              <div className="rect-2">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Procedures</h5>
                </div>
                <p>Awaiting Approval</p>
              </div>
              <div className="rect-3">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Playbook</h5>
                </div>
                <p>Not Approved</p>
              </div>
              <div className="rect-4">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Appetite</h5>
                </div>
                <p>Not Approved</p>
              </div>
            </div>
          </div>

          <div className="info-rect-opacity rect-bottom-left">
            <div className="infoContainer-b-l">
              <div className="rect-1">
                <div>
                  <h5>Policy</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Approved</p>
              </div>
              <div className="rect-2">
                <div>
                  <h5>Procedures</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Awaiting Approval</p>
              </div>
              <div className="rect-3">
                <div>
                  <h5>Playbook</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Not Approved</p>
              </div>
              <div className="rect-4">
                <div>
                  <h5>Appetite</h5>
                  <i className="fa fa-circle"></i>
                </div>
                <p>Not Approved</p>
              </div>
            </div>
          </div>

          <div className="info-rect-opacity rect-bottom-right">
            <div className="infoContainer-b-r">
              <div className="rect-1">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Policy</h5>
                </div>
                <p>Approved</p>
              </div>
              <div className="rect-2">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Procedures</h5>
                </div>
                <p>Awaiting Approval</p>
              </div>
              <div className="rect-3">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Playbook</h5>
                </div>
                <p>Not Approved</p>
              </div>
              <div className="rect-4">
                <div>
                  <i className="fa fa-circle"></i>
                  <h5>Appetite</h5>
                </div>
                <p>Not Approved</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ControlPanel;
