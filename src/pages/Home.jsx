import React from "react";
import undraw_warning from "../assets/images/undraw_warning.png";
import undraw_to_do from "../assets/images/undraw_to_do_list.png";
import { Link, NavLink, useNavigate } from "react-router-dom";

const Home = () => {
    const navigate = useNavigate();

    return (
        <div className="choose-main">
            <div className="container">
                <div className="choose-inner">
                    <p>Choose an option below to continue</p>
                    <h2>How would you like us to help you today?</h2>
                    <div className="check-box-image-main">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="left-img">
                                    <img
                                        src={undraw_warning}
                                        alt="undraw_warning"
                                    />
                                    <div className="bottom-check-box-text">
                                        <h5>IAM IN CRISIS!</h5>
                                        <div className="bottom-part">
                                            <div className="bottom-left">
                                                <span>
                                                    Help me with a step-by step
                                                    checklist of recovery steps
                                                </span>
                                            </div>
                                            <div className="bottom-right">
                                                <div className="img-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        id="crisis"
                                                    />
                                                    <label htmlFor="crisis"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="right-img">
                                    <img
                                        src={undraw_to_do}
                                        alt="undraw_to_do"
                                    />
                                    <div className="bottom-check-box-text">
                                        <h5>BE CRISIS READY</h5>
                                        <div className="bottom-part">
                                            <div className="bottom-left">
                                                <span>
                                                    Help me with a step-by step
                                                    checklist of recovery steps
                                                </span>
                                            </div>
                                            <div className="bottom-right">
                                                <div className="img-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        id="crisis-ready"
                                                    />
                                                    <label htmlFor="crisis-ready"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="text-right">
                                    <Link to="/controlpanel">
                                        <button
                                            type="button"
                                            className="btn btn-primary next-btn"
                                        >
                                            <span>Next</span>
                                            <i
                                                className="fa fa-long-arrow-right"
                                                aria-hidden="true"
                                            ></i>
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
